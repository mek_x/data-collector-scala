scalaVersion := "2.13.3"

name := "DataCollector"
organization := "com.mekops"
version := "0.1.0"
maintainer := "mek.xgt@gmail.com"

enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)
enablePlugins(AshScriptPlugin)

dockerBaseImage := "openjdk:8-jre-alpine"

val AkkaVersion = "2.5.31"

libraryDependencies ++= Seq(
    "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2",
    "com.lightbend.akka" %% "akka-stream-alpakka-mqtt" % "2.0.2",
    "com.typesafe.akka" %% "akka-stream" % AkkaVersion
)
